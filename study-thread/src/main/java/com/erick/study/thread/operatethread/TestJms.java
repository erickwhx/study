package com.erick.study.thread.operatethread;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : Erick
 * @version : 1.0
 * @Description :
 * @time :2019-4-19
 */
public class TestJms {
    static Map<Integer , Runnable> QueueMap = new HashMap<Integer, Runnable>();
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            JmsThread jmsThread = new JmsThread();
            Thread thread = new Thread(jmsThread);
            thread.setName("线程名称:"+i);
            QueueMap.put(i,jmsThread);
            thread.start();
        }


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 9; i++) {
            JmsThread jmsThread = (JmsThread) QueueMap.get(i);
            jmsThread.keepRunning = false;
        }
        System.out.println("线程0-8已经暂停");

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 9; i++) {
            JmsThread jmsThread = (JmsThread) QueueMap.get(i);
            jmsThread.keepRunning = true;
        }

        System.out.println("线程0-8已经启动");
    }
}
