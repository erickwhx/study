package com.erick.study.thread.operatethread;

/**
 * @author : Erick
 * @version : 1.0
 * @Description :
 * @time :2019-4-19
 */
public class JmsThread implements Runnable {

    volatile Boolean keepRunning = true;

    @Override
    public void run() {
        while (true){
            if (keepRunning){
                System.out.println("当前运行线程为：" +Thread.currentThread().getName());
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
