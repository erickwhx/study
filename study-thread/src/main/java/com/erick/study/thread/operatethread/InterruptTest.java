package com.erick.study.thread.operatethread;

    /**
     * @author : Erick
     * @version : 1.0
     * @Description :
     * @time :2019-6-13
     */
    public class InterruptTest {
        public static void main(String[] args) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i <100 ; i++) {
                        System.out.println(Thread.currentThread().getName());
                    }
                }
            });
            thread.setName("aaaaa");
            thread.start();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            thread.interrupt();
            System.out.println("线程已经暂停");
        }
    }
