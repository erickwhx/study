package com.erick.springcloud.configclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RefreshScope
@RestController
public class SpringcloudConfigClientApplication {

    @Value("${java}")
    private String java;

    @Value("${common}")
    private String common;

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudConfigClientApplication.class, args);
    }

    @GetMapping("/sayHello")
    public String  sayHello(){
        System.out.println("say java :" + java);
        System.out.println("common :"+ common);
        return "config-client : "+java+"------common : "+common;
    }
}
